from aws_cdk import (
    aws_iam as iam,
    core
)

class AboutMe(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        iam.Role(
            self, 'About_ME',
            role_name='Neil-Guan' ,
            assumed_by=iam.ServicePrincipal('cathayholdings.com.tw'), 
            description="CathayHoldings DDT  Enginner")

app = core.App()
neil_guan= AboutMe(app, "Neil-Guan-Profile")
core.Tag.add(neil_guan , "a.NAME","Neil Guan", priority=3)
core.Tag.add(neil_guan , "b.EVENT","CDK Meetup Taipei", priority=1)
core.Tag.add(neil_guan , "c.SKILLS","AWS K8S OPENSHIFT", priority=2)
core.Tag.add(neil_guan , "d.EMAIL","neilguan@cathayholdings.com.tw", priority=4)
app.synth()
