from cdk_gitlab_runner import GitlabContainerRunner
from aws_cdk import (
    core, aws_ec2 as ec2,
    aws_iam as iam,
    aws_route53 as r53
)
import os
import requests
# my ip
myip = requests.get('https://checkip.amazonaws.com').text.rstrip()
# need to change your host_zone .
my_hosted_zone = os.environ['NEIL_HOST_ZONE']
# need to change your zone_name .
my_zone_name = os.environ['NEIL_HOST_ZONE_NAME']


class AwsCdkMeetupStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
        # find self account default VPC
        selfvpc = ec2.Vpc.from_lookup(self, 'MyVPC', is_default=True)
        # Create a runner via https://pypi.org/project/cdk-gitlab-runner/
        runner = GitlabContainerRunner(self, 'gitlab-runner', gitlabtoken=os.environ['GITLABTOKEN'],
                                       ec2type="t3.small", tag1='cdk', tag2='meetup', tag3='aws', selfvpc=selfvpc)
        # add another policy to runner runner.runner_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonS3ReadOnlyAccess"))
        # EKS mapping aws-auth config role , ecs deploy policy
        # Let's access lab web .
        runner.runner_ec2.connections.allow_from(
            ec2.Peer.ipv4(myip+'/32'), ec2.Port.tcp(80))
        runner.runner_ec2.connections.allow_from(
            ec2.Peer.ipv4(myip+'/32'), ec2.Port.tcp(443))
        # find my route53 hostzone .
        zone = r53.HostedZone.from_hosted_zone_attributes(
            self, 'MYHOSTED_ZONE', hosted_zone_id=my_hosted_zone, zone_name=my_zone_name)

        # target runner instance public ip .
        newdomain = r53.ARecord(self, "Route53NewArecord", zone=zone,
                                target=r53.RecordTarget.from_ip_addresses(runner.runner_ec2.instance_public_ip), record_name="cdkdemo", ttl=core.Duration.minutes(1))
        # http://cdk-demo.jsregistry.ga
        core.CfnOutput(self, 'Runner-Public-DNS-NAME',
                       value=newdomain.domain_name)
