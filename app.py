#!/usr/bin/env python3

from aws_cdk import core

from aws_cdk_meetup.aws_cdk_meetup_stack import AwsCdkMeetupStack

import os

aws_account = {'account': os.environ['CDK_DEFAULT_ACCOUNT'],
               'region': os.environ['CDK_DEFAULT_REGION']}
app = core.App()
AwsCdkMeetupStack(app, "aws-cdk-meetup", env=aws_account)

app.synth()
